﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Predmetnik.Models
{
    class Employee
    {
        public int ID { get; set; }
	    public string Name { get; set; }
		public decimal Salary { get; set; }
		public int Age { get; set; }
	    public string ProfileImage { get; set; } // file path
    }
}
