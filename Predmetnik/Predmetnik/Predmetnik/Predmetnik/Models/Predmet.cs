﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Predmetnik.Models
{
    class Predmet
    {
        public Guid PredmetId { get; set; }
        public string Naziv { get; set; }
        public string Ects { get; set; }
        public string Semester { get; set; }
    }
}
