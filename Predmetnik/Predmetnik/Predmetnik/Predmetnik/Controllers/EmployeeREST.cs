﻿using Predmetnik.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Predmetnik.Controllers
{
    class EmployeeREST
    {
        private HttpClient httpClient = new HttpClient();
        private const string restUrl = "http://dummy.restapiexample.com/api/v1/employees";
        public List<Employee> VsiZaposleni = new List<Employee>();

        public EmployeeREST()
        {
            this.httpClient.BaseAddress = new Uri(restUrl);
            this.httpClient.MaxResponseContentBufferSize = 256000;
        }

        public HttpClient HttpClient { get => httpClient; }
        public void Bla()
        {
            
        }

        public async Task<List<Employee>> GetEmployeesAsync()
        {
            HttpResponseMessage response = await this.httpClient.GetAsync(restUrl);

            if (response.IsSuccessStatusCode)
            {
                string vsebina = await response.Content.ReadAsStringAsync();
                this.VsiZaposleni = JsonConvert.DeserializeObject<List<Employee>>(vsebina);
            }
            return this.VsiZaposleni;
        }
    }
}
