﻿using Predmetnik.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Predmetnik
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SyllabusPage : ContentPage
    {
        // TODO: Predmetnik
        public SyllabusPage()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            //App.Current.MainPage = new EmployeesPage();
            
            var employeePage = new EmployeesPage();
            employeePage.BindingContext = new EmployeeREST().VsiZaposleni;

            await Navigation.PushAsync(employeePage);
        }
    }
}