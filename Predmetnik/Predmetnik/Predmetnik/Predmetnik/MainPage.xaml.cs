﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Predmetnik
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(true)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        // TODO: Prijava
        private async void BtnLogin_Clicked(object sender, EventArgs e)
        {
            // TODO: Geslo in uporabnisko
            if (this.entryName.Text.Equals("admin") && this.entryPassword.Text.Equals("admin"))
            {
                // TODO: Skok na predmetnik
                await Navigation.PushAsync(new SyllabusPage());
            }
            else
            {
                await DisplayAlert("Nepravilna prijava", "Uporabniško ime ali geslo je nepravilno!", "OK");
            }
        }

        // TODO: Skok na About page
        private async void BtnAbout_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new AboutPage());
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            // TODO: pocistimo vnose, da ne bo uporabnik rabil
            this.entryName.Text = ""; 
            this.entryPassword.Text = "";

            await Task.Delay(1);
            entryName.Focus();
        }
    }
}
